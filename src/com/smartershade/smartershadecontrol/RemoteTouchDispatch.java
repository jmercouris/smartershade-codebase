package com.smartershade.smartershadecontrol;

import android.graphics.Point;

public class RemoteTouchDispatch
{
	static RemoteTouchInterface touchListener = null;
	static String[] touchInputs;
	static int pressed = 1;
	static int state = 0;

	public static void touch(String input)
	{
		if (input.contains("@") && state != pressed)
		{
			state = pressed;
			touchInputs = input.split("@");
			touchInputs = touchInputs[0].split(",");
			try
			{
				touchInputs[1] = touchInputs[1].substring(0, touchInputs[1].indexOf(';'));
			} catch (Exception e)
			{
			}

			Point sendPoint = new Point(Integer.parseInt(touchInputs[0]), Integer.parseInt(touchInputs[1]));
			touchListener.onTouchDown(sendPoint);
		}
		if (input.contains("!") && state == pressed)
		{
			state = pressed + 1;
			touchInputs = input.split("!");
			touchInputs = touchInputs[0].split(",");
			try
			{
				touchInputs[1] = touchInputs[1].substring(0, touchInputs[1].indexOf(';'));
			} catch (Exception e)
			{
			}

			Point sendPoint = new Point(Integer.parseInt(touchInputs[0]), Integer.parseInt(touchInputs[1]));
			touchListener.onTouchUp(sendPoint);
		} else
		{
			touchInputs = input.split(";");
			touchInputs = touchInputs[0].split(",");
			touchListener.onTouch(new Point(Integer.parseInt(touchInputs[0]), Integer.parseInt(touchInputs[1])));
		}
	}

	public void setListener(RemoteTouchInterface listener)
	{
		touchListener = listener;
	}
}
