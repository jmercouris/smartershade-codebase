package com.smartershade.smartershadecontrol;

public class Constant
{
	public static String[] GET_NUMBER_ARRAY(int inputSize, int target, int hand)
	{
		String[] temp = new String[inputSize];
		for (int i = 0; i < inputSize; i++)
		{

			temp[i] = "" + i;
			if (i == target)
			{
				if (hand == LEFT_HAND)
				{
					temp[i] += "\u2192";
				}
				else if (hand == RIGHT_HAND)
				{
					temp[i] = "\u2190" + temp[i];
				}
			}
		}
		return temp;
	}

	public static final String[] CONDITION_NAME = new String[] { 
		"Kalibrierung vorn/links", "Kalibrierung vorn/rechts", "Kalibrierung hinten/links", "Kalibrierung hinten/rechts",
		"Pointing vorn/links", "Pointing vorn/rechts", "Pointing hinten/links", "Pointing hinten/rechts", 
		"Scroll vertical/vorn/links/mapping 1", "Scroll vertical/vorn/links/mapping 2", "Scroll vertical/vorn/links/mapping 3",
		"Scroll vertical/vorn/rechts/mapping 1", "Scroll vertical/vorn/rechts/mapping 2", "Scroll vertical/vorn/rechts/mapping 3",
		"Scroll horizontal/vorn/links/mapping 1", "Scroll horizontal/vorn/links/mapping 2", "Scroll horizontal/vorn/links/mapping 3"};
	public static final String DELIMITER = ";";
	public static final String NEW_LINE = "\n";
	public static String WRITE_DIRECTORY = "/InteractiveMaps";
	public static String REMOTE_IP = "";
	public static boolean GLOVE_RECORDING;
	public static final int CONDITION_COUNT = CONDITION_NAME.length;
	public static boolean[] CONDITION_RUN = new boolean[CONDITION_COUNT];
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd-HH-mm-ss";
	public static String PRACTICE = "";
	public static final int DEFAULT_MARGIN = 1;

	public static final int UPPER_AGE_BOUND = 120;
	public static final int GRID_ROWS = 7;
	public static final int GRID_COLUMNS = 10;
	public static final int HOME_SIZE = 28;
	public static final int TOUCH_SIZE = 13;
	public static final int CALIBRATION_TRIALS = 20 + 1;
	public static final int CALIBRATION_CONIDTION_COUNT = 4;
	public static final String CSV_SUFFIX = ".csv";

	public static final int HORIZONTAL_REACH = 6;
	public static final int VERTICAL_REACH = 7;
	public static int TRIALS_PER_AREA = 5;

	public static final int[] TOUCH_SIZES = new int[] { 14, 21, 28 };
	public static final float DEFAULT_TRANSLUCENCY = .4f;

	// Scroll Conditions
	public static final int[] SCROLL_DISTANCES = new int[] { 10, 20, 40, -10, -20, -40 };
	public static final float[] SCROLL_VELOCITIES = new float[] { 1.0f, 4.0f, 3.0f };
	public static final int TRIALS_PER_SCROLL = 4;

	// Used for Assigning States to Conditions
	public static final int MAPPINGDEFAULT = 0;
	public static final int MAPPINGMULTIPLY = 1;
	public static final int MAPPINGPYTHAGORAS = 2;

	// Message types sent from the BluetoothChatService Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// Key names received from the BluetoothChatService Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// Intent request codes
	public static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
	public static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
	public static final int REQUEST_ENABLE_BT = 3;

	// Used for Assigning States to Conditions
	public static final int LEFT_HAND = 0;
	public static final int RIGHT_HAND = 1;

	// Arrow Details
	public static final int ARROW_SIDE_LENGTH = 60;
	public static final int ARROWLEFT = 0;
	public static final int ARROWRIGHT = 1;
	public static final int ARROWTOP = 2;
	public static final int ARROWBOTTOM = 3;
	public static final int ARROWONSCREEN = 4;

	// Participant Varialbes
	public static final int GENDER_MAN = 0;
	public static final int GENDER_WOMAN = 1;
	public static final int GENDER_NA = 2;

	public static final int HANDEDNESS_LEFT = 0;
	public static final int HANDEDNESS_RIGHT = 1;
	public static final int HANDEDNESS_AMBIDEXTROUS = 2;
	
	public static int EXPERIMENT_0 = 0;
	public static int EXPERIMENT_1 = 1;
	public static int EXPERIMENT_2 = 2;
	public static int EXPERIMENT_3 = 3;
	public static int EXPERIMENT = 0;

}
