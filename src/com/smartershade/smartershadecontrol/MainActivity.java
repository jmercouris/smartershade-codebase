package com.smartershade.smartershadecontrol;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
//import android.os.SystemClock;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
//import android.view.View;
//import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

public class MainActivity extends Activity 
{
	// Class Variables
	int previousSeekBarValue;
	// Bounds of Servo movement
	int minServoPosition = 35;
	int maxServoPosition = 170;
	// Servo Value Backwards Direction to remove strain
	int servoTrim = 3;
	// Seek Bar range variable
	int seekBarOffset = 0;
	// Layout Controls
	Button minSubtract, minAdd, maxSubtract, maxAdd;
	SeekBar seekbar;

	
	///////////////////////////////////////////////////////////////////////////
	// On Create
	///////////////////////////////////////////////////////////////////////////
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		//---------------------------------------------------------------------
		// Get Bluetooth adapter
		//---------------------------------------------------------------------
		BluetoothSession.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (BluetoothSession.bluetoothAdapter == null)
		{
			Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		
		// ---------------------------------------------------------------------
		// Setup Calibration Handlers
		// ---------------------------------------------------------------------
		/*
		minSubtract = (Button) findViewById(R.id.button1);
		minAdd = (Button) findViewById(R.id.button2);
		maxSubtract = (Button) findViewById(R.id.button3);
		maxAdd = (Button) findViewById(R.id.button4);
		
		minSubtract.setOnClickListener(new OnClickListener() 
		{ 
			@Override
			public void onClick(View arg0) 
			{
				if(minServoPosition > 0)
					minServoPosition--;
				setSeekBarBounds(minServoPosition, maxServoPosition, seekbar);
			}
		});
		minAdd.setOnClickListener(new OnClickListener() 
		{ 
			@Override
			public void onClick(View arg0) 
			{
				if(minServoPosition < maxServoPosition)
					minServoPosition++;
				setSeekBarBounds(minServoPosition, maxServoPosition, seekbar);
			}
		});
		maxSubtract.setOnClickListener(new OnClickListener() 
		{ 
			@Override
			public void onClick(View arg0) 
			{
				if(maxServoPosition > minServoPosition)
					maxServoPosition--;
				setSeekBarBounds(minServoPosition, maxServoPosition, seekbar);
			}
		});
		maxAdd.setOnClickListener(new OnClickListener() 
		{ 
			@Override
			public void onClick(View arg0) 
			{
				if(maxServoPosition < 180)
					maxServoPosition++;
				setSeekBarBounds(minServoPosition, maxServoPosition, seekbar);
			}
		});

		*/
		// ---------------------------------------------------------------------
		// Seekbar Handling & Setup
		// ---------------------------------------------------------------------	
		seekbar = (SeekBar) findViewById(R.id.seekBar1);
		setSeekBarBounds(minServoPosition, maxServoPosition, seekbar);
		seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
		{
		    @Override
		    public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {}
		    @Override
		    public void onStartTrackingTouch(SeekBar seekBar) {}

		    @Override
		    public void onStopTrackingTouch(SeekBar seekBar) 
		    {
		    	int seekbarOutput = seekbar.getProgress();
		    	//int seekbarTrim = 0;
		    	seekbarOutput += seekBarOffset;
				sendMessage(seekbarOutput +";");
				System.out.println(seekbarOutput);
				/*
				// Send Trim Value
				SystemClock.sleep(500);
				if (seekbarOutput > previousSeekBarValue)
				{
			    	seekbarTrim = seekbarOutput - servoTrim;
			    	sendMessage(seekbarTrim +";");
				}
				if (seekbarOutput < previousSeekBarValue)
				{
					seekbarTrim = seekbarOutput + servoTrim;
					sendMessage(seekbarTrim +";");
				}	
				*/
				previousSeekBarValue = seekbarOutput;
		    }
		});
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Set Seek Bar Bounds
	///////////////////////////////////////////////////////////////////////////
	public void setSeekBarBounds(int min, int max, SeekBar seekBarInput)
	{
		seekBarOffset = min;
		seekBarInput.setMax(max - min);
	}

	///////////////////////////////////////////////////////////////////////////
	// Options Menu
	///////////////////////////////////////////////////////////////////////////
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_menu, menu);
		return true;
	}

	///////////////////////////////////////////////////////////////////////////
	// Options Menu
	///////////////////////////////////////////////////////////////////////////
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		Intent serverIntent = null;
		switch (item.getItemId())
		{
		case R.id.insecure_connect_scan:
			serverIntent = new Intent(this, DeviceListActivity.class);
			startActivityForResult(serverIntent, Constant.REQUEST_CONNECT_DEVICE_INSECURE);
			return true;
		/*case R.id.discoverable:
			ensureDiscoverable();
			return true;
		*/
		}
		return false;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Ensure Discover-able
	///////////////////////////////////////////////////////////////////////////
	@SuppressWarnings("unused")
	private void ensureDiscoverable()
	{
		if (BluetoothSession.bluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)
		{
			Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			startActivity(discoverableIntent);
		}
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Device Connection
	///////////////////////////////////////////////////////////////////////////
	private void connectDevice(Intent data, boolean secure)
	{
		// Get the device MAC address
		String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
		// Get the BluetoothDevice object
		BluetoothDevice device = BluetoothSession.bluetoothAdapter.getRemoteDevice(address);
		// Attempt to connect to the device
		BluetoothSession.blueToothService.connect(device, secure);
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Setup Connection Handler
	///////////////////////////////////////////////////////////////////////////
	private void setupConnection()
	{
		// Initialize the BluetoothChatService to perform bluetooth connections
		BluetoothSession.blueToothService = new BluetoothService(this, mHandler);
		System.out.println("setup chat called");
		// Initialize the buffer for outgoing messages
		BluetoothSession.outStringBuffer = new StringBuffer("");
	}

	///////////////////////////////////////////////////////////////////////////
	// Handler
	///////////////////////////////////////////////////////////////////////////
	// The Handler that gets information back from the BluetoothChatService
	//-------------------------------------------------------------------------
	private final Handler mHandler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
			case Constant.MESSAGE_STATE_CHANGE:
				switch (msg.arg1)
				{
				case BluetoothService.STATE_CONNECTED:
					break;
				case BluetoothService.STATE_CONNECTING:
					break;
				case BluetoothService.STATE_LISTEN:
					break;
				case BluetoothService.STATE_NONE:
					break;
				}
				break;

			case Constant.MESSAGE_WRITE:
				//byte[] writeBuf = (byte[]) msg.obj;
				// construct a string from the buffer
				//String writeMessage = new String(writeBuf);
				break;
			case Constant.MESSAGE_READ:
				byte[] readBuf = (byte[]) msg.obj;
				String readMessage = new String(readBuf, 0, msg.arg1);

				try
				{
					RemoteTouchDispatch.touch(readMessage);
				} catch (Exception e)
				{
				}
				break;
			case Constant.MESSAGE_DEVICE_NAME:
				// Save the connected device's name
				BluetoothSession.connectedDeviceName = msg.getData().getString(Constant.DEVICE_NAME);
				Toast.makeText(getApplicationContext(), "Connected to " + BluetoothSession.connectedDeviceName, Toast.LENGTH_SHORT).show();
				break;
			case Constant.MESSAGE_TOAST:
				Toast.makeText(getApplicationContext(), msg.getData().getString(Constant.TOAST), Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	///////////////////////////////////////////////////////////////////////////
	// Activity Result
	///////////////////////////////////////////////////////////////////////////
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode)
		{
		case Constant.REQUEST_CONNECT_DEVICE_SECURE:
			// When DeviceListActivity returns with a device to connect
			if (resultCode == Activity.RESULT_OK)
			{
				connectDevice(data, true);
			}
			break;
		case Constant.REQUEST_CONNECT_DEVICE_INSECURE:
			// When DeviceListActivity returns with a device to connect
			if (resultCode == Activity.RESULT_OK)
			{
				connectDevice(data, false);
			}
			break;
		case Constant.REQUEST_ENABLE_BT:
			// When the request to enable Bluetooth returns
			if (resultCode == Activity.RESULT_OK)
			{
				setupConnection();
			} else
			{
				// User did not enable Bluetooth or an error occurred
				Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}

	@Override
	///////////////////////////////////////////////////////////////////////////
	// On Destroy
	///////////////////////////////////////////////////////////////////////////
	public void onDestroy()
	{
		super.onDestroy();
		// Stop the Bluetooth chat services
		if (BluetoothSession.blueToothService != null)
			BluetoothSession.blueToothService.stop();
	}

	@Override
	///////////////////////////////////////////////////////////////////////////
	// On Resume
	///////////////////////////////////////////////////////////////////////////
	public synchronized void onResume()
	{
		super.onResume();
		if (BluetoothSession.blueToothService != null)
		{ // Only if the state is

			if (BluetoothSession.blueToothService.getState() == BluetoothService.STATE_NONE)
			{
				BluetoothSession.blueToothService.start();
			}
		}
	}

	@Override
	///////////////////////////////////////////////////////////////////////////
	// On Start
	///////////////////////////////////////////////////////////////////////////
	public void onStart()
	{
		super.onStart();
		// If BT is not on, request that it be enabled.
		// setupChat() will then be called during onActivityResult
		if (!BluetoothSession.bluetoothAdapter.isEnabled())
		{
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, Constant.REQUEST_ENABLE_BT);
			// Otherwise, setup the chat session
		} else
		{
			if (BluetoothSession.blueToothService == null)
				setupConnection();
		}
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Send Message
	///////////////////////////////////////////////////////////////////////////
	private void sendMessage(String message)
	{
		// Check that we're actually connected before trying anything
		if (BluetoothSession.blueToothService.getState() != BluetoothService.STATE_CONNECTED)
		{
			return;
		}

		// Check that there's actually something to send
		if (message.length() > 0)
		{
			// Get the message bytes and tell the BluetoothChatService to write
			byte[] send = message.getBytes();
			BluetoothSession.blueToothService.write(send);

			// Reset out string buffer to zero and clear the edit text field
			BluetoothSession.outStringBuffer.setLength(0);
			
		}
	}
}
