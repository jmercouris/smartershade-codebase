package com.smartershade.smartershadecontrol;

import java.util.EventListener;

import android.graphics.Point;

public interface RemoteTouchInterface extends EventListener
{
	void onTouch(Point point);
	void onTouchUp(Point point);
	void onTouchDown(Point point);
}
