#include <Servo.h> 
#include <SoftwareSerial.h> 

#define RxD         2
#define TxD         3

SoftwareSerial blueToothSerial(RxD,TxD);
Servo myServo;


/////////////////////////////////////////////////
// Setup
/////////////////////////////////////////////////
void setup()
{
  Serial.begin(19200);
  pinMode(RxD, INPUT);
  pinMode(TxD, OUTPUT);
  myServo.attach(9);
  setupBlueToothConnection();
}

/////////////////////////////////////////////////
// Main Loop
/////////////////////////////////////////////////
void loop()
{
  char recvChar;
  String inputBuffer = "";
  
  while(1)
  {
    // Check if Bluetooth is Available
    if(blueToothSerial.available())
    {
      //Check if there's any data sent from the remote bluetooth shield
      recvChar = blueToothSerial.read();
      /////////////////////////////////////////////////
      // Input Ending- Perform Operations
      /////////////////////////////////////////////////
      if(recvChar == ';')
      {
        int interpretedValue = inputBuffer.toInt();
        Serial.print(inputBuffer);
        
        /*
        if (interpretedValue <= 20)
        {
          interpretedValue = 20;
        }
        if (interpretedValue >= 160)
        {
          interpretedValue = 160;
        }
        */

        myServo.write(interpretedValue);
        Serial.println(interpretedValue);
        inputBuffer = "";
      }
      /////////////////////////////////////////////////
      // Add to Input Buffer
      /////////////////////////////////////////////////
      else
      {
        inputBuffer += recvChar;
      }
    }
  }
}


void setupBlueToothConnection()
{
  blueToothSerial.begin(38400);                           // Set BluetoothBee BaudRate to default baud rate 38400
  blueToothSerial.print("\r\n+STWMOD=0\r\n");             // set the bluetooth work in slave mode
  blueToothSerial.print("\r\n+STNA=SmarterShade\r\n");    // set the bluetooth name as "SeeedBTSlave"
  blueToothSerial.print("\r\n+STOAUT=1\r\n");             // Permit Paired device to connect me
  blueToothSerial.print("\r\n+STAUTO=0\r\n");             // Auto-connection should be forbidden here
  delay(2000);                                            // This delay is required.
  blueToothSerial.print("\r\n+INQ=1\r\n");                // make the slave bluetooth inquirable
  Serial.println("The slave bluetooth is inquirable!");
  delay(2000);                                            // This delay is required.
  blueToothSerial.flush();
}

